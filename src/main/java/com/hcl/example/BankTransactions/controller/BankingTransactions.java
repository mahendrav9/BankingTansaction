package com.hcl.example.BankTransactions.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.example.BankTransactions.model.BankMainTransactions;
import com.hcl.example.BankTransactions.service.TransactionServiceRbs;

@RestController
public class BankingTransactions {

	@Autowired
	TransactionServiceRbs transactionServiceRbs;

	@RequestMapping(value = "/hello", method = RequestMethod.GET)
	public ResponseEntity<String> sample() {
		return new ResponseEntity<>("hello world", HttpStatus.OK);

	}
	

	@RequestMapping(value = "/alltransactions", method = RequestMethod.GET, produces = "application/json")
	public List<BankMainTransactions> listOfTransactions() {
		return transactionServiceRbs.getAllTransactions();

	}

}
