package com.hcl.example.BankTransactions.model;

public class BankMainTransactions {

	private String bookingDateTime;

	private String counterPartyLogoPath;

	private String transactionAmount;

	private String accountId;

	private String location;

	private String categoryId;

	private String creditDebitIndicator;

	private String remittanceInformation;

	private String counterpartyName;

	private String instructedCurrency;

	private String id;

	private String counterpartyAccount;

	private String instructedAmount;

	private String transactionType;

	private String address;

	private String description;

	private String merchantType;

	private String transactionCurrency;

	public String getBookingDateTime() {
		return bookingDateTime;
	}

	public void setBookingDateTime(String bookingDateTime) {
		this.bookingDateTime = bookingDateTime;
	}

	public String getCounterPartyLogoPath() {
		return counterPartyLogoPath;
	}

	public void setCounterPartyLogoPath(String counterPartyLogoPath) {
		this.counterPartyLogoPath = counterPartyLogoPath;
	}

	public String getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(String transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getCreditDebitIndicator() {
		return creditDebitIndicator;
	}

	public void setCreditDebitIndicator(String creditDebitIndicator) {
		this.creditDebitIndicator = creditDebitIndicator;
	}

	public String getRemittanceInformation() {
		return remittanceInformation;
	}

	public void setRemittanceInformation(String remittanceInformation) {
		this.remittanceInformation = remittanceInformation;
	}

	public String getCounterpartyName() {
		return counterpartyName;
	}

	public void setCounterpartyName(String counterpartyName) {
		this.counterpartyName = counterpartyName;
	}

	public String getInstructedCurrency() {
		return instructedCurrency;
	}

	public void setInstructedCurrency(String instructedCurrency) {
		this.instructedCurrency = instructedCurrency;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCounterpartyAccount() {
		return counterpartyAccount;
	}

	public void setCounterpartyAccount(String counterpartyAccount) {
		this.counterpartyAccount = counterpartyAccount;
	}

	public String getInstructedAmount() {
		return instructedAmount;
	}

	public void setInstructedAmount(String instructedAmount) {
		this.instructedAmount = instructedAmount;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMerchantType() {
		return merchantType;
	}

	public void setMerchantType(String merchantType) {
		this.merchantType = merchantType;
	}

	public String getTransactionCurrency() {
		return transactionCurrency;
	}

	public void setTransactionCurrency(String transactionCurrency) {
		this.transactionCurrency = transactionCurrency;
	}

	@Override
	public String toString() {
		return "ClassPojo [bookingDateTime = " + bookingDateTime + ", counterPartyLogoPath = " + counterPartyLogoPath
				+ ", transactionAmount = " + transactionAmount + ", accountId = " + accountId + ", location = "
				+ location + ", categoryId = " + categoryId + ", creditDebitIndicator = " + creditDebitIndicator
				+ ", remittanceInformation = " + remittanceInformation + ", counterpartyName = " + counterpartyName
				+ ", instructedCurrency = " + instructedCurrency + ", id = " + id + ", counterpartyAccount = "
				+ counterpartyAccount + ", instructedAmount = " + instructedAmount + ", transactionType = "
				+ transactionType + ", address = " + address + ", description = " + description + ", merchantType = "
				+ merchantType + ", transactionCurrency = " + transactionCurrency + "]";
	}
}
