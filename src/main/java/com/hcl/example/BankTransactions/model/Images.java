package com.hcl.example.BankTransactions.model;

public class Images {

	private String id;

	private String label;

	private String URL;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String URL) {
		this.URL = URL;
	}

	@Override
	public String toString() {
		return "ClassPojo [id = " + id + ", label = " + label + ", URL = " + URL + "]";
	}
}
