package com.hcl.example.BankTransactions.model;

public class Physical_location {

	private String longitude;

	private String latitude;

	private String date;

	private User user;

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "ClassPojo [longitude = " + longitude + ", latitude = " + latitude + ", date = " + date + ", user = "
				+ user + "]";
	}

}
