package com.hcl.example.BankTransactions.service;

import java.util.List;

import com.hcl.example.BankTransactions.model.BankMainTransactions;


public interface TransactionServiceRbs {
	
	public List<BankMainTransactions> getAllTransactions();
	public Double getTransactionByType(String type);

}
