package com.hcl.example.BankTransactions.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.hcl.example.BankTransactions.model.BankMainTransactions;
import com.hcl.example.BankTransactions.model.MainTransactions;
import com.hcl.example.BankTransactions.model.Transactions;

@Service
public class TransactionServiceRbsImpl implements TransactionServiceRbs {

	@Override
	public List<BankMainTransactions> getAllTransactions() {
		ResponseEntity<MainTransactions> response = getTransactions();

		List<BankMainTransactions> bankMainTransactions = new ArrayList<BankMainTransactions>();

		for (Transactions mainTransactions : response.getBody().getTransactions()) {

			BankMainTransactions bankMainTransaction = new BankMainTransactions();
			bankMainTransaction.setId(mainTransactions.getId());
			bankMainTransaction.setAccountId(mainTransactions.getThis_account().getId());
			bankMainTransaction.setCounterpartyAccount(mainTransactions.getOther_account().getNumber());
			bankMainTransaction.setCounterpartyName(mainTransactions.getOther_account().getHolder().getName());
			bankMainTransaction
					.setCounterPartyLogoPath(mainTransactions.getOther_account().getMetadata().getImage_URL());
			bankMainTransaction.setInstructedAmount(mainTransactions.getDetails().getValue().getAmount());
			bankMainTransaction.setInstructedCurrency(mainTransactions.getDetails().getValue().getCurrency());
			bankMainTransaction.setTransactionAmount(mainTransactions.getDetails().getValue().getAmount());
			bankMainTransaction.setTransactionCurrency(mainTransactions.getDetails().getValue().getCurrency());
			bankMainTransaction.setTransactionType(mainTransactions.getDetails().getType());
			bankMainTransaction.setDescription(mainTransactions.getDetails().getDescription());
			bankMainTransactions.add(bankMainTransaction);

		}

		return bankMainTransactions;
	}

	private ResponseEntity<MainTransactions> getTransactions() {
		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = "https://apisandbox.openbankproject.com/obp/v1.2.1/banks/rbs/accounts/savings-kids-john/public/transactions";
		return restTemplate.getForEntity(fooResourceUrl, MainTransactions.class);

	}

	@Override
	public Double getTransactionByType(String type) {
		return (double) 0;
	}

}
