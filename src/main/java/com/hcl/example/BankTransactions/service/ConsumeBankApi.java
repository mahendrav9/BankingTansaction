package com.hcl.example.BankTransactions.service;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.hcl.example.BankTransactions.model.MainTransactions;

public class ConsumeBankApi {

	public static void main(String args[]) {

		RestTemplate restTemplate = new RestTemplate();
		String fooResourceUrl = "https://apisandbox.openbankproject.com/obp/v1.2.1/banks/rbs/accounts/savings-kids-john/public/transactions";
		ResponseEntity<MainTransactions> response = restTemplate.getForEntity(fooResourceUrl, MainTransactions.class);
		System.out.println("hello......."+response);

	}

}
